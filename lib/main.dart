// ignore_for_file: unused_import

import 'package:e_commerce/controllers/popular_product_controller.dart';
import 'package:e_commerce/controllers/recomended_product.dart';
import 'package:e_commerce/routes/route.dart';
import 'package:e_commerce/screen/Food/popular_detail.dart';
import 'package:e_commerce/screen/Food/recomended_detail.dart';
import 'package:e_commerce/screen/Pages/main_food.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'helper/dependencies.dart' as dep;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dep.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Get.find<PopularProductController>().getPopularProductList();
    Get.find<RecomendedProductController>().getRecomendedProductList();
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyFoodMain(),
      initialRoute: RouteHelper.initial,
      getPages: RouteHelper.routes,
    );
  }
}
