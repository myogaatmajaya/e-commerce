import 'package:e_commerce/screen/Food/popular_detail.dart';
import 'package:e_commerce/screen/Food/recomended_detail.dart';
import 'package:e_commerce/screen/Pages/main_food.dart';
import 'package:get/get.dart';

class RouteHelper {
  static const String initial = '/';
  static const String popularFood = '/popular-food';
  static const String recommendedFood = '/recommended-food';
  static const String cartPage = '/cart-page';
  static const String foodPage = '/food-page';

  static String getInitial() => '$initial';
  static String getPopularFood(int pageId) => '$popularFood?pageId=$pageId';
  static String getRecommendedFood(int pageId) =>
      '$recommendedFood?pageId=$pageId';
  static String getCartPage() => cartPage;
  static String getFoodPage(int pageId) => '$foodPage?pageId=$pageId';

  static List<GetPage> routes = [
    GetPage(
      name: initial,
      page: () => MyFoodMain(),
    ),
    GetPage(
      name: popularFood,
      page: () {
        var pageId = Get.parameters['pageId'];
        return PopularFoodDetail(pageId: int.parse(pageId!));
      },
      transition: Transition.upToDown,
    ),
    GetPage(
        name: recommendedFood,
        page: () {
          var pageId = Get.parameters['pageId'];
          return RecomendedFoodDetail(pageId: int.parse(pageId!));
        },
        transition: Transition.downToUp,
        transitionDuration: Duration(milliseconds: 300)),
    // GetPage(name: cartPage, page: () => CartPage()),
    // GetPage(name: foodPage, page: () => FoodPage()),
  ];
}
