import 'package:flutter/material.dart';

class AppIcon extends StatelessWidget {
  final IconData icon;
  final Color backgroundColor;
  final Color iconColor;
  final double iconsize;
  const AppIcon(
      {super.key,
      required this.icon,
      this.backgroundColor = const Color(0xFFfcf4e4),
      this.iconColor = const Color(0xFF756d54),
      this.iconsize = 40});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: iconsize,
      height: iconsize,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(iconsize / 2),
          color: backgroundColor),
      child: Icon(
        icon,
        color: iconColor,
        size: 16,
      ),
    );
  }
}
