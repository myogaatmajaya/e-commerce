import 'package:flutter/material.dart';

class AppColors {
  // Light Color
  static final Color colorLightPrimary = const Color(0xFF5C93C4);
  static const Color colorLightSecondary = Color(0xFFF9F6E5);
  static const Color colorLightCardColors = Color(0xFFFFFFFF);
  // #Light Color

// Light Color
  static const Color colorDarkPrimary = Color(0xFF222831);
  static const Color colorDarkSecondary = Color(0xFF30475E);
  static const Color colorDarkThird = Color(0xFFF2A365);
  static const Color colorDarkTitle = Color(0xFFECECEC);
  // #Light Color

  // Get Started
  static const Color colorStarted = Color(0xFF274C71);
  static const Color colorStartedTitle = Color(0xFF352641);
  static const Color colorStartedDescription = Color(0xFF767676);
  static const Color colorStartedShadow = Color(0x60274C71);
  // #Get Started

  // Alert Dialog
  static const Color colorAlertDialogBack = Color(0xFFF8F7F2);
  // #Alert Dialog

  static const Color colorPrimaryLightBlue = Color.fromARGB(255, 118, 244, 255);
  static const Color colorPrimaryDarkBlue = const Color(0xff0039cb);

  static const Color colorPrimaryRed = const Color(0xffd50000);
  static const Color colorPrimaryLightRed = const Color(0xffff5131);
  static const Color colorPrimaryDarkRed = const Color(0xff9b0000);

  static const Color colorPrimaryGreen = const Color(0xff00c853);
  static const Color colorPrimaryLightGreen = const Color(0xff5efc82);
  static const Color colorPrimaryDarkGreen = const Color(0xff009624);

  static const Color colorPrimaryPurple = const Color(0xffaa00ff);
  static const Color colorPrimaryLightPurple = const Color(0xffe254ff);
  static const Color colorPrimaryDarkPurple = const Color(0xff7200ca);

  static const Color colorPrimaryDarkYellow = const Color(0xffffd600);
  static const Color colorPrimaryDarkLightYellow = const Color(0xffffff52);
  static const Color colorPrimaryDarkDarkYellow = const Color(0xffc7a500);

  static const Color colorBronze = const Color(0xFFD6943A);
  static const Color colorSilver = const Color(0xFFD9D9D9);
  static const Color colorGold = const Color(0xFFF7D44E);

  static const Color buttonBackground = Color(0xFFEFEFEF);

  static const Color signColor = Color(0xFFEFEFEF);

  static const Color mainColor = Color(0xFF89dad0);
}
