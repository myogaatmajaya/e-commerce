import 'package:e_commerce/screen/Pages/food_body.dart';
import 'package:e_commerce/Widegts/big_text.dart';
import 'package:e_commerce/Widegts/small_text.dart';
import 'package:e_commerce/utils/appcolor.dart';
import 'package:flutter/material.dart';

class MyFoodMain extends StatefulWidget {
  const MyFoodMain({super.key});

  @override
  State<MyFoodMain> createState() => _MyFoodMainState();
}

class _MyFoodMainState extends State<MyFoodMain> {
  @override
  Widget build(BuildContext context) {
    print("height is" + MediaQuery.of(context).size.height.toString());
    print("width is" + MediaQuery.of(context).size.width.toString());
    return Scaffold(
        body: Column(
      children: [
        Container(
          child: Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(left: 10, right: 10, top: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    BigText(
                      text: 'FoodieShop',
                      color: AppColors.colorPrimaryLightBlue,
                    ),
                    Row(
                      children: [
                        SmallText(
                          text: 'Category',
                          color: AppColors.colorDarkSecondary,
                        ),
                        Icon(Icons.arrow_drop_down),
                      ],
                    ),
                  ],
                ),
                Center(
                  child: Container(
                    width: 45,
                    height: 45,
                    child: Icon(Icons.search, color: Colors.white),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: AppColors.colorPrimaryLightBlue),
                  ),
                )
              ],
            ),
          ),
        ),
        Expanded(
            child: SingleChildScrollView(
          child: FoodBody(),
        )),
      ],
    ));
  }
}
