// ignore_for_file: unused_import

import 'package:e_commerce/Widegts/app_column.dart';
import 'package:e_commerce/Widegts/big_text.dart';
import 'package:e_commerce/Widegts/small_text.dart';
import 'package:e_commerce/controllers/popular_product_controller.dart';
import 'package:e_commerce/controllers/recomended_product.dart';
import 'package:e_commerce/models/product_models.dart';
import 'package:e_commerce/routes/route.dart';
import 'package:e_commerce/screen/Food/popular_detail.dart';
import 'package:e_commerce/utils/appConstants.dart';
import 'package:e_commerce/utils/appcolor.dart';
import 'package:e_commerce/utils/dimensions.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:get/get.dart';

class FoodBody extends StatefulWidget {
  const FoodBody({super.key});

  @override
  State<FoodBody> createState() => _FoodBodyState();
}

class MyCustomScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}

class _FoodBodyState extends State<FoodBody> {
  PageController pageController = PageController(viewportFraction: 0.85);
  var _currentPageValue = 0.0;
  final double _scaleFactor = 0.8;
  final double _height = 220;
  @override
  void initState() {
    super.initState();
    pageController.addListener(() {
      setState(() {
        _currentPageValue = pageController.page!;
      });
    });
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      GetBuilder<PopularProductController>(builder: (popularProducts) {
        return popularProducts.isLoaded
            ? Container(
                // color: Colors.grey,
                height: Dimensions.pageView,

                child: PageView.builder(
                  scrollBehavior: MyCustomScrollBehavior(),
                  controller: pageController,
                  itemCount: popularProducts.popularProductList.length,
                  itemBuilder: (context, position) {
                    return _buildPageItem(
                        position, popularProducts.popularProductList[position]);
                  },
                ),
              )
            : CircularProgressIndicator(
                color: AppColors.colorPrimaryLightBlue,
              );
      }),
      // ignore: unnecessary_new
      GetBuilder<PopularProductController>(builder: (popularProducts) {
        return DotsIndicator(
          dotsCount: popularProducts.popularProductList.isEmpty
              ? 1
              : popularProducts.popularProductList.length,
          position: _currentPageValue,
          decorator: DotsDecorator(
            activeColor: AppColors.colorPrimaryLightBlue,
            size: const Size.square(9.0),
            activeSize: const Size(18.0, 9.0),
            activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
          ),
        );
      }),
      SizedBox(height: Dimensions.height30),
      Container(
        margin: EdgeInsets.only(left: Dimensions.width30),
        child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
          BigText(text: "Recomended"),
          SizedBox(
            width: Dimensions.width10,
          ),
          Container(
            margin: EdgeInsets.only(bottom: 3),
            child: BigText(text: ".", color: Colors.black26),
          ),
          SizedBox(
            width: Dimensions.width10,
          ),
          Container(
            margin: EdgeInsets.only(bottom: 2),
            child: SmallText(text: "Food pairing"),
          ),
        ]),
      ),

      GetBuilder<RecomendedProductController>(builder: (recomendedProduct) {
        return recomendedProduct.isLoaded
            ? ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: recomendedProduct.recomendedProductList.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Get.toNamed(RouteHelper.getRecommendedFood(index));
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          left: Dimensions.width20,
                          right: Dimensions.width20,
                          top: 10),
                      child: Row(children: [
                        Container(
                          width: Dimensions.listViewImgSize,
                          height: Dimensions.listViewImgSize,
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius20),
                            color: Colors.white38,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(AppConstants.BASE_URL +
                                    AppConstants.UPLOAD_URL +
                                    recomendedProduct
                                        .recomendedProductList[index].img!)),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            height: 100,
                            padding: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topRight:
                                      Radius.circular(Dimensions.radius20),
                                  bottomRight:
                                      Radius.circular(Dimensions.radius20)),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding:
                                  EdgeInsets.only(left: Dimensions.width10),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    BigText(
                                        text: recomendedProduct
                                            .recomendedProductList[index]
                                            .name!),
                                    SizedBox(height: Dimensions.height10),
                                    SmallText(
                                        text: "With Chinese characteristics"),
                                    SizedBox(height: Dimensions.height10),
                                    //       Row(
                                    //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    //         children: [
                                    //           IconAndTextWidget(
                                    //               icon: Icons.circle_sharp,
                                    //               text: "Normal",
                                    //               iconColor: AppColors.iconColor1),
                                    //           IconAndTextWidget(
                                    //               icon: Icons.location_on,
                                    //               text: "1.7km",
                                    //               iconColor: AppColors.mainColor),
                                    //         ]
                                    // )
                                  ]),
                            ),
                          ),
                        )
                      ]),
                    ),
                  );
                },
              )
            : CircularProgressIndicator(
                color: AppColors.colorPrimaryLightBlue,
              );
      })
    ]);
  }

  Widget _buildPageItem(int index, ProductModel popularProduct) {
    Matrix4 matrix = new Matrix4.identity();
    if (index == _currentPageValue.floor()) {
      var currScale = 1 - (_currentPageValue - index) * (1 - _scaleFactor);
      var currTrans = _height * (1 - currScale) / 2;
      matrix = Matrix4.diagonal3Values(1, currScale, 1)
        ..setTranslationRaw(0, currTrans, 0);
    } else if (index == _currentPageValue.floor() + 1) {
      var currScale =
          _scaleFactor + (_currentPageValue - index + 1) * (1 - _scaleFactor);
      var currTrans = _height * (1 - currScale) / 2;
      matrix = Matrix4.diagonal3Values(1, currScale, 1)
        ..setTranslationRaw(0, currTrans, 0);
    } else if (index == _currentPageValue.floor() - 1) {
      var currScale = 1 - (_currentPageValue - index) * (1 - _scaleFactor);
      var currTrans = _height * (1 - currScale) / 2;
      matrix = Matrix4.diagonal3Values(1, currScale, 1)
        ..setTranslationRaw(0, currTrans, 0);
    } else {
      var currScale = 0.8;
      matrix = Matrix4.diagonal3Values(1, currScale, 1)
        ..setTranslationRaw(0, _height * (1 - _scaleFactor) / 2, 1);
    }

    return Transform(
      transform: matrix,
      child: Stack(
        children: [
          GestureDetector(
            onTap: () {
              Get.toNamed(RouteHelper.getPopularFood(index));
            },
            child: Container(
              margin: EdgeInsets.only(
                  left: Dimensions.width10,
                  right: Dimensions.height10,
                  top: 10),
              height: Dimensions.pageViewContainer,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: index.isEven
                    ? Color(0xFF69C5DF)
                    : Color.fromARGB(255, 64, 66, 69),
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(AppConstants.BASE_URL +
                        AppConstants.UPLOAD_URL +
                        popularProduct.img!)),
              ),
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.only(left: 25, right: 25, bottom: 10),
                height: Dimensions.pageViewTextContainer,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      // spreadRadius: 5,
                      blurRadius: 5,
                      color: Color(0xFFe8e8e8),
                      offset: Offset(0, 2),
                    ) // changes position of shadow
                  ],
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white,
                ),
                child: Container(
                  padding: EdgeInsets.only(top: 10, left: 15, right: 15),
                  child: AppColumn(text: popularProduct.name!),
                ),
              ))
        ],
      ),
    );
  }
}
