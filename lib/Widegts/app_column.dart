import 'package:e_commerce/Widegts/big_text.dart';
import 'package:e_commerce/Widegts/small_text.dart';
import 'package:e_commerce/utils/appcolor.dart';
import 'package:e_commerce/utils/dimensions.dart';
import 'package:flutter/material.dart';

class AppColumn extends StatelessWidget {
  final String text;
  const AppColumn({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: Dimensions.height10,
        ),
        BigText(
          text: text,
          size: Dimensions.font26,
        ),
        Row(
          children: [
            Wrap(
              children: List.generate(
                  5,
                  (index) => Icon(
                        Icons.star,
                        color: AppColors.colorPrimaryLightBlue,
                      )),
            ),
            SizedBox(
              width: 10,
            ),
            SmallText(text: "5.0"),
          ],
        ),
        SizedBox(
          width: 10,
        ),
        SmallText(text: "Very Chips and Delicious"),
      ],
    );
  }
}
