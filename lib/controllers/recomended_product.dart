import 'package:e_commerce/models/product_models.dart';
import 'package:get/get.dart';
import 'package:e_commerce/data/repository/recomended_product_repo.dart';

class RecomendedProductController extends GetxController {
  final RecomendedProductRepo recomendedProductRepo;

  RecomendedProductController({required this.recomendedProductRepo});

  List<ProductModel> recomendedProductList = [];

  List<ProductModel> get popularProductList => recomendedProductList;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  Future<void> getRecomendedProductList() async {
    Response response = await recomendedProductRepo.getRecomendedProductList();
    if (response.statusCode == 200) {
      print("got products recomended");
      final Map<String, dynamic> responseBody = response.body;

      if (responseBody.containsKey('products')) {
        final List<dynamic> productsJson = responseBody['products'];
        recomendedProductList = productsJson
            .map((productJson) => ProductModel.fromJson(productJson))
            .toList();

        _isLoaded = true;
        update();
      } else {
        // Handle the case when 'products' key is not present in the response
      }
    } else {
      // Handle error scenario
    }
  }
}
